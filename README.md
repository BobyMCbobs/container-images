# container-images

> A bunch of container images that I've put together for various purposes

# Contents

In this repo is:

- [kubectl-validate](./apps/kubectl-validate/)
- [octodns](./apps/octodns/)
- [reveal-multiplex](./apps/reveal-multiplex/)
- [postgres](./apps/postgres/)
- [age](./apps/age/)
- [apko](./apps/apko/)
- [openssh-client](./apps/openssh-client)

# Production

Each container image is tagged with the format of *vCOMMIT_TIMESTAMP-COMMIT_HASH* such as *v1710753832-f6572b1e*. This format is useful for looking through a list of tags to find the most recent one and what commit it corresponds to.

Images, importantly, are also signed with [Cosign](https://docs.sigstore.dev/signing/quickstart/) in order to verify integrity and providence.
