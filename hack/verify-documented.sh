#!/bin/sh

set -o errexit
set -o nounset

cd "$(git rev-parse --show-toplevel)" || exit 1

FAIL=false
for APP in $(find apps -mindepth 1 -maxdepth 1 -type d | xargs); do
    if ! grep -q "$APP" ./README.md; then
        echo "$APP not found in README.md"
        FAIL=true
    fi
    if [ ! -f "$APP/README.md" ]; then
        echo "$APP/README.md not found"
        FAIL=true
    fi
done

if [ "$FAIL" = true ]; then
    exit 1
fi
