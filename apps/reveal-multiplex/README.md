# reveal-multiplex

a rewrite and custom build of [reveal/multiplex](https://github.com/reveal/multiplex) server written in Deno, instead of NodeJS for increased speed and safety.
