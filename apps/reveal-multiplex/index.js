import http from 'node:http';
import fs from 'node:fs';
import dayjs from 'npm:dayjs@1.11.10';
import * as mod from "https://deno.land/std@0.157.0/hash/sha256.ts";
import express from 'npm:express@4.18.2';
import morgan from 'npm:morgan@1.10.0'
import cors from 'npm:cors@2.8.5'
import { Server } from 'npm:socket.io@4.7.1';
let app         = express();
let staticDir   = express.static;
let server      = http.createServer(app);
const dateFormat = ':date[web] :method :url :status :res[content-length] - :response-time ms'

app.use(cors())
app.use(morgan(dateFormat))
let io = new Server(server, {
  cors: {
    origin: '*',
    methods: ["GET", "POST", "PUT", "DELETE", "OPTIONS" ]
  }
});

io.engine.use(morgan(dateFormat))

function shutdown(signal) {
  console.log(`${signal} signal received: closing HTTP server`)
  server.close(() => {
    console.log('HTTP server closed')
  })
}

function date() {
  return dayjs().format('ddd, DD MMM YYYY HH:MM:ss')
}

const opts = {
  port: Deno.env.get("PORT") || 1948,
  baseDir: Deno.cwd()
};
const createHash = secret => {
  return new mod.Sha256().update(secret).hex()
};

io.on( 'connection', socket => {
  console.log(`${date()} socket: new connection`)
  socket.on('multiplex-statechanged', data => {
    if (typeof data.secret == 'undefined' || data.secret == null || data.secret === '') return;
    console.log(`${date()} multiplex-statechanged: ${data.socketId} => ${JSON.stringify(data.state)}`)
    if (createHash(data.secret) === data.socketId) {
      data.secret = null;
      socket.broadcast.emit(data.socketId, data);
    } else {
      console.warn(`socketId and secret do not match: ${data.socketId}`)
    };
  });
});

app.use( express.static( opts.baseDir ) );

app.get("/", ( req, res ) => {
  res.writeHead(200, {'Content-Type': 'text/html'});

  let stream = fs.createReadStream( opts.baseDir + '/index.html' );
  stream.on('error', error => {
    res.write('<style>body{font-family: sans-serif;}</style><h2>reveal.js multiplex server.</h2><a href="/token">Generate token</a>');
    res.end();
  });
  stream.on('open', () => {
    stream.pipe( res );
  });
});

app.get("/token", ( req, res ) => {
  let ts = new Date().getTime();
  let rand = Math.floor(Math.random()*9999999);
  let secret = ts.toString() + rand.toString();
  res.send({secret: secret, socketId: createHash(secret)});
});

['SIGTERM', 'SIGINT'].map(s => {
  Deno.addSignalListener(s, () => shutdown(s))
})

server.listen( opts.port || null );
console.log(`reveal.js: Multiplex running on port ${opts.port}`);
