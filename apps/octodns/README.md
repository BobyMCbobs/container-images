# octodns

a container build of [octodns](https://github.com/octodns/octodns) with the DigitalOcean provider included, inspired by [kubernetes/k8s.io/dns](https://github.com/kubernetes/k8s.io/tree/main/dns).
